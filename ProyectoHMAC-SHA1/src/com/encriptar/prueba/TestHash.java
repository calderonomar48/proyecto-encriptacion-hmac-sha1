package com.encriptar.prueba;

import java.security.MessageDigest;
import java.security.SignatureException;
import java.util.Base64;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Hex;

public class TestHash {
	
	public static void main(String[] args) {
		
		MessageDigest md = null;
		String pass = "123456";
		
		try {
		
			String cadenaEncriptada = TestHash.Signature("Test message", pass) ; 
		
			System.out.println( cadenaEncriptada );
			
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	public static String Signature(String xData, String AppKey) throws java.security.SignatureException {
		try {
			// obtenemos la referencia del encode de Base64
			final Base64.Encoder encoder = Base64.getEncoder();
			
			// Encriptamos la clave: primero lo convertirmo a un arreglo de bytes y luego lo pasamos al formato HMAC-sha1
			SecretKeySpec signingKey = new SecretKeySpec(AppKey.getBytes("UTF-8"),"HmacSHA1");

			// Obtenemos una instancia para nuestro formato de encriptacion
			Mac mac = Mac.getInstance("HmacSHA1");
			mac.init(signingKey);

			// El cuerpo del mensaje tambi�n lo convertimos a un arreglo de bytes y lo mandamos a encriptar
			byte[] rawHmac = mac.doFinal(xData.getBytes("UTF-8"));
			String result = encoder.encodeToString(rawHmac);
			return result;
			
			// dentro del paquete crypto existe la clase Mac.
			// De la clase Mac usamos los metodos init y doFinal

		} catch (Exception e) {
			throw new SignatureException("Failed to generate HMAC : "+ e.getMessage());
		}
	}
	
}
